<?php

namespace Drupal\content_redirect_to_front\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Redirects to the frontpage based on the settings.
 *
 * @package Drupal\content_redirect_to_front\EventSubscriber
 */
class RedirectSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  const PATTERN_GROUPNAME = 'content_type';

  const PATTERN = "/entity\.(?P<" . self::PATTERN_GROUPNAME . ">.+)\.canonical/U";

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Path matcher service.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * RedirectSubscriber constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   Current user.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Language manager.
   * @param \Drupal\Core\Path\PathMatcherInterface $pathMatcher
   *   Path matcher service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(AccountProxyInterface $currentUser, LanguageManagerInterface $languageManager, PathMatcherInterface $pathMatcher, ConfigFactoryInterface $configFactory, MessengerInterface $messenger) {
    $this->currentUser = $currentUser;
    $this->languageManager = $languageManager;
    $this->pathMatcher = $pathMatcher;
    $this->configFactory = $configFactory;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Dynamic Page Cache service has a priority of 27.
    // We need to set bigger than that to add a redirect if needed.
    $events[KernelEvents::REQUEST][] = ['onKernelRequest', 28];
    return $events;
  }

  /**
   * Sets redirect if needed.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The Event to process.
   */
  public function onKernelRequest(RequestEvent $event) {
    $request = $event->getRequest();

    $response = $this->checkRedirect($request);

    if (isset($response)) {
      $event->setResponse($response);
    }
  }

  /**
   * Checks if redirect should be applied based on the route and config.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse|null
   *   TrustedRedirectResponse if redirect should be applied. NULL otherwise.
   */
  protected function checkRedirect(Request $request) {
    if ($this->pathMatcher->isFrontPage()) {
      return NULL;
    }

    $routeName = $request->attributes->get('_route');

    $config = $this->configFactory->get('content_redirect_to_front.settings');
    $entityTypeSettings = $config->get('enabled_types') ?? [];
    $bundleSettings = $config->get('bundle_settings') ?? [];

    $matches = [];
    if (preg_match(self::PATTERN, $routeName, $matches)) {
      $contentType = $matches[self::PATTERN_GROUPNAME];

      // If content type is not enabled in redirection settings,
      // then we don't need to do anything.
      if (empty($entityTypeSettings[$contentType])) {
        return NULL;
      }

      $content = $request->attributes->get($contentType);

      // At this point we only need to check if there are
      // bundle limitations set. If no specific bundles are set,
      // then the redirection should apply to any bundle this entity type has.
      if (!$this->isRedirectLimitedByBundle($bundleSettings, $contentType)) {
        return $this->getRedirectResult($config);
      }

      // Otherwise check for bundle.
      if ($content instanceof EntityInterface) {
        $bundle = $content->bundle();

        if (!empty($bundleSettings[$contentType][$bundle])) {
          return $this->getRedirectResult($config);
        }

      }

    }

    return NULL;
  }

  /**
   * Checks for bundle limitations.
   *
   * If no specific bundles are set, then the redirection should apply
   * to any bundle this entity type has.
   *
   * @param string[] $bundleSettings
   *   Bundle settings from config.
   * @param string $contentType
   *   Content type ID.
   *
   * @return bool
   *   Returns TRUE if no specific bundles are set, FALSE otherwise.
   */
  protected function isRedirectLimitedByBundle(array $bundleSettings, $contentType) {
    if (empty($bundleSettings) || empty($contentType) || empty($bundleSettings[$contentType])) {
      return FALSE;
    }

    foreach ($bundleSettings[$contentType] as $bundleSetting) {
      if (!empty($bundleSetting)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Returns the redirect response.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *   Redirects to the frontpage for the current language.
   */
  protected function getRedirectResult($config) {
    if ($this->currentUser->hasPermission('skip redirecting to front for all content')) {
      if (!empty($config->get('message_settings.message_enabled'))) {
        $messageContent = !empty($config->get('message_settings.message_content')) ?
          $config->get('message_settings.message_content') :
          $this->t('This entity is configured to redirect to the front page, but because you have the permission to skip this redirect, you are allowed to view this page.');
        $this->messenger->addWarning($messageContent);
      }

      return NULL;
    }

    $langcode = $this->languageManager->getCurrentLanguage();
    return new TrustedRedirectResponse(
      Url::fromUserInput('/', ['language' => $langcode])->toString()
    );
  }

}
