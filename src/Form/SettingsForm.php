<?php

namespace Drupal\content_redirect_to_front\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings for the content_redirect_to_front module.
 *
 * @package Drupal\content_redirect_to_front\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   Entity type bundle info service.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigmanager
   *   The typed config manager.
   */
  public function __construct(ConfigFactoryInterface $configFactory, EntityTypeManagerInterface $entityTypeManager, EntityTypeBundleInfoInterface $entityTypeBundleInfo, TypedConfigManagerInterface $typedConfigmanager) {
    parent::__construct($configFactory, $typedConfigmanager);
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->typedConfigManager = $typedConfigmanager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('config.typed')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_redirect_to_front_setting_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config($this->getEditableConfigNames()[0]);
    $options = [];
    $definitions = $this->entityTypeManager->getDefinitions();
    $bundles = $this->entityTypeBundleInfo->getAllBundleInfo();

    $form['#tree'] = TRUE;

    $form['enabled_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Entities to redirect'),
      '#default_value' => $config->get('enabled_types') ?? [],
      '#description' => $this->t('Select which entity types should be redirected to the front page from their canonical link. If an entity type is checked, the redirect will be applied to every bundle of that type by default. You can fine-tune redirect settings for bundles with <em>Bundle specific settings</em>.'),
    ];

    $form['bundle_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Bundle specific settings'),
    ];

    foreach ($definitions as $entityTypeId => $definition) {
      $currentOptions = [];
      $options[$definition->id()] = $definition->getLabel();

      if (!empty($bundles[$entityTypeId])) {
        foreach ($bundles[$entityTypeId] as $bundleId => $bundle) {
          $currentOptions[$bundleId] = $bundle['label'];
        }

        $form['bundle_settings'][$entityTypeId] = [
          '#type' => 'checkboxes',
          '#options' => $currentOptions,
          '#title' => $definition->getLabel(),
          '#description' => $this->t('If the entity type is checked and no element is selected here, then every bundle will be redirected. If the entity type is not checked, no redirect will be applied to the front page.'),
          '#default_value' => $config->get('bundle_settings.' . $entityTypeId) ?? [],
        ];
      }

    }

    $form['enabled_types']['#options'] = $options;

    $form['message_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Message settings'),
    ];

    $form['message_settings']['message_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display a message about the redirect for users that may skip the redirect'),
      '#default_value' => $config->get('message_settings.message_enabled') ?? FALSE,
    ];

    $form['message_settings']['message_content'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message content'),
      '#default_value' => $config->get('message_settings.message_content') ?? $this->t('This entity is configured to redirect to the front page, but because you have the permission to skip this redirect, you are allowed to view this page.'),
      '#states' => [
        'visible' => [
          ':input[name="message_settings[message_enabled]"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config($this->getEditableConfigNames()[0]);
    $config->set('enabled_types', $form_state->getValue('enabled_types'));
    $config->set('bundle_settings', $form_state->getValue('bundle_settings'));
    $config->set('message_settings', $form_state->getValue('message_settings'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['content_redirect_to_front.settings'];
  }

}
