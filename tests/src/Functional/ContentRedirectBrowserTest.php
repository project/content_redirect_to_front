<?php

namespace Drupal\Tests\content_redirect_to_front\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Test basic functionality of My Module.
 *
 * @group content_redirect_to_front
 */
class ContentRedirectBrowserTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'content_redirect_to_front',
    'user',
    'language',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    // Make sure to complete the normal setup steps first.
    parent::setUp();

    $this->drupalCreateContentType([
      'type' => 'page',
      'name' => 'Basic page',
      'display_submitted' => FALSE,
    ]);
    $this->drupalCreateContentType([
      'type' => 'article',
      'name' => 'Article',
      'display_submitted' => FALSE,
    ]);

    $this->grantPermissions(
      Role::load(RoleInterface::AUTHENTICATED_ID),
      ['skip redirecting to front for all content']
    );
    // Don't set anything to config at start.
  }

  /**
   * Unset every redirection.
   */
  protected function unsetEverything() {
    \Drupal::configFactory()
      ->getEditable('content_redirect_to_front.settings')
      ->set('enabled_types', [
        'node' => '0',
      ])
      ->set('bundle_settings', [
        'node' => [
          'page' => '0',
          'article' => '0',
        ],
      ])
      ->set('message_settings', [
        'message_enabled' => FALSE,
        'message_content' => '',
      ])
      ->save(TRUE);
  }

  /**
   * Set redirect to all nodes.
   */
  protected function setAllNodeRedirect() {
    \Drupal::configFactory()
      ->getEditable('content_redirect_to_front.settings')
      ->set('enabled_types', [
        'node' => '1',
      ])
      ->set('bundle_settings', [
        'node' => [
          'page' => '0',
          'article' => '0',
        ],
      ])
      ->set('message_settings', [
        'message_enabled' => FALSE,
        'message_content' => '',
      ])
      ->save(TRUE);
  }

  /**
   * Set default module configuration for tests.
   */
  protected function setArticleNodeRedirect() {
    \Drupal::configFactory()
      ->getEditable('content_redirect_to_front.settings')
      ->set('enabled_types', [
        'node' => '1',
      ])
      ->set('bundle_settings', [
        'node' => [
          'page' => '0',
          'article' => '1',
        ],
      ])
      ->set('message_settings', [
        'message_enabled' => FALSE,
        'message_content' => '',
      ])
      ->save(TRUE);
  }

  /**
   * Unset bundle specific settings.
   */
  protected function unsetRedirectWithBundleSetting() {
    \Drupal::configFactory()
      ->getEditable('content_redirect_to_front.settings')
      ->set('enabled_types', [
        'node' => '0',
      ])
      ->set('bundle_settings', [
        'node' => [
          'page' => '0',
          'article' => '0',
        ],
      ])
      ->set('message_settings', [
        'message_enabled' => FALSE,
        'message_content' => '',
      ])
      ->save(TRUE);
  }

  /**
   * Clear caches.
   */
  protected function clearCache() {
    drupal_flush_all_caches();
  }

  /**
   * Make sure the site still works. For now just check the front page.
   */
  public function testRedirectWithDifferentSettings() {
    $node1 = Node::create(['type' => 'page', 'title' => 'First']);
    $node1->save();

    $node2 = Node::create(['type' => 'article', 'title' => 'Second']);
    $node2->save();

    // After installing module.
    $this->drupalGet('node/' . $node1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('node/' . $node1->id());

    $this->drupalGet('node/' . $node2->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('node/' . $node2->id());

    // After not setting anything.
    $this->unsetEverything();
    $this->clearCache();

    $this->drupalGet('node/' . $node1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('node/' . $node1->id());

    $this->drupalGet('node/' . $node2->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('node/' . $node2->id());

    // After setting just the bundle without the entity type.
    $this->unsetRedirectWithBundleSetting();
    $this->clearCache();

    $this->drupalGet('node/' . $node1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('node/' . $node1->id());

    $this->drupalGet('node/' . $node2->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('node/' . $node2->id());

    // After setting all the Node to redirect.
    $this->setAllNodeRedirect();
    $this->clearCache();

    $this->drupalGet('node/' . $node1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('/');

    $this->drupalGet('node/' . $node2->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('/');

    // After setting just the article node to redirect.
    $this->setArticleNodeRedirect();
    $this->clearCache();

    $this->drupalGet('node/' . $node1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('node/' . $node1->id());

    $this->drupalGet('node/' . $node2->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('/');

    // Check if the skip redirecting permission works.
    $user = $this->drupalCreateUser([], 'user');
    $this->drupalLogin($user);
    $this->clearCache();

    $this->drupalGet('node/' . $node2->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('node/' . $node2->id());
    $this->drupalLogout();

    // Install another language.
    ConfigurableLanguage::createFromLangcode('hu')->save();

    // Enable locale module.
    $this->container
      ->get('module_installer')
      ->install(['locale', 'content_translation']);
    $this->resetAll();

    $this->drupalLogin($this->rootUser);

    // Set the article content type to use multilingual support.
    $this->drupalGet("admin/structure/types/manage/article");
    $this->assertSession()->pageTextContains('Language settings');
    $edit = [
      'language_configuration[content_translation]' => 1,
    ];
    $this->submitForm($edit, 'Save');

    // Create node3 in English.
    $node3 = Node::create([
      'langcode' => 'en',
      'type' => 'article',
      'title' => 'Third',
    ]);
    $node3->save();

    // Create translation for node3.
    $node3Translated = $node3->addTranslation('hu', [
      'title' => 'Third Hu',
    ]);
    $node3Translated->save();

    // Check if anonymous user is redirected to the front page at the same
    // language.
    $this->drupalLogout();
    $this->drupalGet('hu/node/' . $node3->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('hu');

    $this->drupalGet('node/' . $node3->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('/');
  }

  /**
   * Set redirect to all nodes.
   */
  protected function setAllNodeRedirectWithMessage() {
    \Drupal::configFactory()
      ->getEditable('content_redirect_to_front.settings')
      ->set('enabled_types', [
        'node' => '1',
      ])
      ->set('bundle_settings', [
        'node' => [
          'page' => '0',
          'article' => '0',
        ],
      ])
      ->set('message_settings', [
        'message_enabled' => TRUE,
        'message_content' => 'This will be redirected for else.',
      ])
      ->save(TRUE);
  }

  /**
   * Set redirect to all nodes.
   */
  protected function setAllNodeRedirectWithEmptyMessage() {
    \Drupal::configFactory()
      ->getEditable('content_redirect_to_front.settings')
      ->set('enabled_types', [
        'node' => '1',
      ])
      ->set('bundle_settings', [
        'node' => [
          'page' => '0',
          'article' => '0',
        ],
      ])
      ->set('message_settings', [
        'message_enabled' => TRUE,
        'message_content' => '',
      ])
      ->save(TRUE);
  }

  /**
   * Redirect message should display only for those who are not redirected.
   */
  public function testRedirectMessageSettings() {
    $node1 = Node::create([
      'type' => 'page',
      'title' => 'First',
      'status' => 1,
    ]);
    $node1->save();

    $node2 = Node::create([
      'type' => 'article',
      'title' => 'Second',
      'status' => 1,
    ]);
    $node2->save();

    // 1. with message.
    $this->setAllNodeRedirectWithMessage();
    $this->clearCache();

    // All nodes should be redirected with no message.
    $this->drupalGet('node/' . $node1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('/');

    $this->drupalGet('node/' . $node2->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('/');

    // Login as user that can skip redirects.
    $user = $this->drupalCreateUser(['skip redirecting to front for all content'], 'user');
    $this->drupalLogin($user);

    // For the user the message should be shown with no redirects.
    $this->drupalGet('node/' . $node1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('node/' . $node1->id());
    $this->assertSession()->statusMessageContains('This will be redirected for else.');

    $this->drupalGet('node/' . $node2->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('node/' . $node2->id());
    $this->assertSession()->statusMessageContains('This will be redirected for else.');

    // @todo we might wanna check with config translation here.
    $this->drupalLogout();

    // 2. without message.
    $this->setAllNodeRedirectWithEmptyMessage();
    $this->clearCache();

    // All nodes should be redirected with no message.
    $this->drupalGet('node/' . $node1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('/');

    $this->drupalGet('node/' . $node2->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('/');

    // Login as user who can skip redirects.
    $this->drupalLogin($user);

    // For the user the message should be shown with no redirects.
    $this->drupalGet('node/' . $node1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('node/' . $node1->id());
    $this->assertSession()->pageTextContains('This entity is configured to redirect to the front page, but because you have the permission to skip this redirect, you are allowed to view this page.');

    $this->drupalGet('node/' . $node2->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('node/' . $node2->id());
    $this->assertSession()->pageTextContains('This entity is configured to redirect to the front page, but because you have the permission to skip this redirect, you are allowed to view this page.');

    $this->drupalLogout();
  }

}
