#Content Redirect to Front

Redirects from the selected content types' canonical page to the frontpage.

Useful when you're listing content in a block and you don't want 
those contents have a separate page
accessible (/node/nid or /taxonomy/term/tid etc.).
The content itself will be visible to the users if used elsewhere,
this module just redirects from the canonical URL to the front page
for the entity types (and bundles).

The settings are saved to the site configuration.

## Install

- Install the module. You'll need Drupal 9.0 or newer.
- Go to /admin/config/content/content_redirect_to_front_settings form to set
which content types and bundles should be redirected to the front page when
the visitors open their canonical URL.
- You can set 2 permissions for this module:
  - Skip redirecting to front for all content: if a user has this permission,
  the redirects that were set on the settings form won't be applied.
  The admin user will behave like this. That means if Anonymous users don't have
  this permission, the redirects will be applied for them.
  - Access content_redirect_to_front settings form: this gives access to the
  redirection settings form.
